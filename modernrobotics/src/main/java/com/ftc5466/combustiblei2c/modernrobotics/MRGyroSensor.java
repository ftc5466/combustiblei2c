/*
 * Copyright (c) 2016 The Combustible Lemons - FTC Team 5466 <http://ftc5466.com>
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.ftc5466.combustiblei2c.modernrobotics;

import com.ftc5466.combustiblei2c.I2CRequest;
import com.qualcomm.robotcore.hardware.GyroSensor;
import com.qualcomm.robotcore.hardware.I2cDevice;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class MRGyroSensor extends MRI2CDriver implements GyroSensor {
    public static final int DEFAULT_I2C_ADDRESS = 0x20;

    private volatile int command;
    private volatile int heading, integratedZ, x, y, z, zOffset, zScalingCoeffecient;

    public MRGyroSensor(I2cDevice device, int i2cAddress) {
        super(device, i2cAddress);
    }

    public MRGyroSensor(I2cDevice device) {
        this(device, DEFAULT_I2C_ADDRESS);
    }

    @Override
    public void calibrate() {
        queueWrite(COMMAND_REGISTER, (byte) 0x4E);
    }

    @Override
    public boolean isCalibrating() {
        return command == 0x4E;
    }

    @Override
    public int getHeading() {
        return heading;
    }

    @Override
    public double getRotation() {
        return Double.NaN;
    }

    public int getIntegratedZValue() {
        return integratedZ;
    }

    @Override
    public int rawX() {
        return x;
    }

    @Override
    public int rawY() {
        return y;
    }

    @Override
    public int rawZ() {
        return z;
    }

    @Override
    public void resetZAxisIntegrator() {
        queueWrite(COMMAND_REGISTER, (byte) 0x52);
    }

    @Override
    public String status() {
        return "Modern Robotics Gyro (CombustibleI2C) | " + i2cDevice.getController().getSerialNumber() + " - " + i2cDevice.getPort();
    }

    @Override
    public String getDeviceName() {
        return "Modern Robotics Gyro";
    }

    @Override
    public String getConnectionInfo() {
        return i2cDevice.getConnectionInfo();
    }

    @Override
    public int getVersion() {
        return 0;
    }

    @Override
    protected void noRequests() {
        readData();
    }

    @Override
    protected void reply(I2CRequest request, byte[] data) {
        if (request.register == 0x03 && data.length >= 15) {
            ByteBuffer buf = ByteBuffer.wrap(data);
            buf.order(ByteOrder.LITTLE_ENDIAN);
            command = buf.get() & 0xFF;
            heading = buf.getShort();
            integratedZ = buf.getShort();
            x = buf.getShort();
            y = buf.getShort();
            z = buf.getShort();
            zOffset = buf.getShort();
            zScalingCoeffecient = buf.getShort();
            readData();
        }
    }

    private void readData() {
        queueRead(0x03, 15);
    }
}
