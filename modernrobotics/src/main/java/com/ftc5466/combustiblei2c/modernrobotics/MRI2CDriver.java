/*
 * Copyright (c) 2016 The Combustible Lemons - FTC Team 5466 <http://ftc5466.com>
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.ftc5466.combustiblei2c.modernrobotics;

import android.support.annotation.CallSuper;

import com.ftc5466.combustiblei2c.I2CDriver;
import com.ftc5466.combustiblei2c.I2CRequest;
import com.qualcomm.robotcore.hardware.I2cDevice;

public abstract class MRI2CDriver extends I2CDriver {
    public static final int COMMAND_REGISTER = 0x03;

    protected MRI2CDriver(I2cDevice device, int i2cAddress) {
        super(device, i2cAddress, 0);
    }

    public void changeI2CAddress(int newAddress) {
        if (newAddress % 2 != 0) throw new IllegalArgumentException("Address must be multiple of 2");
        queueWrite(0x70, new byte[] {(byte) newAddress, 0x55, (byte) 0xAA});
    }

    @CallSuper
    @Override
    protected void doneWrite(I2CRequest request) {
        if (request.register == 0x70) {
            setI2cAddress(request.buffer[0] & 0xFF);
        }
    }
}
