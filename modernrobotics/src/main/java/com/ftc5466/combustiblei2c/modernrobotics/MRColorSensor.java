/*
 * Copyright (c) 2016 The Combustible Lemons - FTC Team 5466 <http://ftc5466.com>
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.ftc5466.combustiblei2c.modernrobotics;

import com.ftc5466.combustiblei2c.I2CRequest;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.I2cDevice;

public class MRColorSensor extends MRI2CDriver implements ColorSensor {
    public static final int DEFAULT_I2C_ADDRESS = 0x3C;

    private volatile int command;
    private volatile int color, red, green, blue, alpha;

    public MRColorSensor(I2cDevice device, int i2cAddress) {
        super(device, i2cAddress);
    }

    public MRColorSensor(I2cDevice device) {
        this(device, DEFAULT_I2C_ADDRESS);
    }

    public int colorNumber() {
        return color;
    }

    @Override
    public int red() {
        return red;
    }

    @Override
    public int green() {
        return green;
    }

    @Override
    public int blue() {
        return blue;
    }

    @Override
    public int alpha() {
        return alpha;
    }

    @Override
    public int argb() {
        return (alpha << 24) | (red << 16) | (green << 8) | blue;
    }

    @Override
    public void enableLed(boolean enable) {
        queueWrite(COMMAND_REGISTER, (byte) (enable ? 0x00 : 0x01));
    }

    public void calibrateBlackLevel() {
        queueWrite(COMMAND_REGISTER, (byte) 0x42);
    }

    public boolean isCalibratingBlackLevel() {
        return getMode() == Mode.BLACK_LEVEL_CALIBRATION;
    }

    public void calibrateWhiteLevel() {
        queueWrite(COMMAND_REGISTER, (byte) 0x43);
    }

    public boolean isCalibratingWhiteLevel() {
        return getMode() == Mode.WHITE_LEVEL_CALIBRATION;
    }

    public void setFrequency(Frequency frequency) {
        queueWrite(COMMAND_REGISTER, (byte) (frequency == Frequency._50HZ ? 0x35 : 0x36));
    }

    public boolean isSettingFrequency() {
        Mode m = getMode();
        return m == Mode.SETTING_FREQUENCY_TO_50HZ || m == Mode.SETTING_FREQUENCY_TO_60HZ;
    }

    public void setMode(Mode mode) {
        if (mode != Mode.UNKNOWN) {
            queueWrite(COMMAND_REGISTER, mode.value());
        }
    }

    public Mode getMode() {
        return Mode.forValue(command);
    }

    @Override
    public String getDeviceName() {
        return "Modern Robotics Color Sensor";
    }

    @Override
    public String getConnectionInfo() {
        return i2cDevice.getConnectionInfo();
    }

    @Override
    public int getVersion() {
        return 0;
    }

    @Override
    protected void noRequests() {
        readData();
    }

    @Override
    protected void reply(I2CRequest request, byte[] data) {
        if (request.register == 0x03 && data.length >= 6) {
            command = data[0] & 0xFF;
            color = data[1] & 0xFF;
            red = data[2] & 0xFF;
            green = data[3] & 0xFF;
            blue = data[4] & 0xFF;
            alpha = data[5] & 0xFF;
            readData();
        }
    }

    private void readData() {
        queueRead(0x03, 6);
    }

    public enum Mode {
        UNKNOWN(-1), ACTIVE(0x00), PASSIVE(0x01), BLACK_LEVEL_CALIBRATION(0x42),
        WHITE_LEVEL_CALIBRATION(0x43), SETTING_FREQUENCY_TO_50HZ(0x35),
        SETTING_FREQUENCY_TO_60HZ(0x36);

        private final byte value;

        Mode(int value) {
            this.value = (byte) value;
        }

        public byte value() {
            return value;
        }

        public static Mode forValue(int value) {
            for (Mode m : values())
                if (value == m.value())
                    return m;
            return UNKNOWN;
        }
    }

    public enum Frequency {
        _50HZ, _60HZ
    }
}
