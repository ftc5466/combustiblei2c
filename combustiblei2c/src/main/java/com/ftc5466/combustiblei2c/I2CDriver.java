/*
 * Copyright (c) 2016 The Combustible Lemons - FTC Team 5466 <http://ftc5466.com>
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.ftc5466.combustiblei2c;

import com.qualcomm.robotcore.hardware.I2cController;
import com.qualcomm.robotcore.hardware.I2cDevice;

import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class I2CDriver implements I2cController.I2cPortReadyCallback {
    protected final I2cDevice i2cDevice;
    private int i2cAddress;
    private final int i2cAddressReadFlag;
    private final ConcurrentLinkedQueue<I2CRequest> queue = new ConcurrentLinkedQueue<>();

    protected I2CDriver(I2cDevice device, int i2cAddress, int i2cAddressReadFlag) {
        i2cDevice = device;
        this.i2cAddress = i2cAddress;
        this.i2cAddressReadFlag = i2cAddressReadFlag;
        i2cDevice.registerForI2cPortReadyCallback(this);
    }

    public void setI2cAddress(int address) {
        i2cAddress = address;
    }

    public int getI2cAddress() {
        return i2cAddress;
    }

    protected void queue(I2CRequest request) {
        request.state = I2CRequest.RequestState.QUEUED;
        queue.add(request);
    }

    protected void queueWrite(int register, byte[] data) {
        queue(I2CRequest.newWrite(register, data));
    }

    protected void queueWrite(int register, byte datum) {
        queueWrite(register, new byte[] {datum});
    }

    protected void queueRead(int register, int bytes) {
        queue(I2CRequest.newRead(register, bytes));
    }

    public void close() {
        i2cDevice.deregisterForPortReadyCallback();
        i2cDevice.close();
    }

    @Override
    public final void portIsReady(int port) {
        if (queue.isEmpty()) {
            noRequests();
        } else {
            I2CRequest request = queue.peek();
            if (request.state == I2CRequest.RequestState.PENDING_READ) {
                i2cDevice.readI2cCacheFromController();
                request.state = I2CRequest.RequestState.READING;
            } else {
                if (request.state == I2CRequest.RequestState.WRITING) {
                    doneWrite(request);
                } else if (request.state == I2CRequest.RequestState.READING) {
                    reply(request, i2cDevice.getCopyOfReadBuffer());
                }

                if (request.state != I2CRequest.RequestState.QUEUED) {
                    request.state = I2CRequest.RequestState.DONE;
                    queue.poll();
                    if (queue.isEmpty()) return;
                    request = queue.peek();
                }

                if (request.type == I2CRequest.RequestType.WRITE) {
                    i2cDevice.enableI2cWriteMode(i2cAddress, request.register, request.bytes);
                    i2cDevice.copyBufferIntoWriteBuffer(request.buffer);
                    request.state = I2CRequest.RequestState.WRITING;
                } else {
                    i2cDevice.enableI2cReadMode(i2cAddress | i2cAddressReadFlag, request.register, request.bytes);
                    request.state = I2CRequest.RequestState.PENDING_READ;
                }
                i2cDevice.setI2cPortActionFlag();
                i2cDevice.writeI2cCacheToController();
            }
        }
    }

    protected abstract void noRequests();

    protected abstract void doneWrite(I2CRequest request);

    protected abstract void reply(I2CRequest request, byte[] data);
}
